package com.example.star_war.resistencia;

import com.example.star_war.resistencia.traitor.TraitorController;
import com.example.star_war.resistencia.report.Report;
import com.example.star_war.resistencia.report.IReportService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TraitorController.class)
public class RelarorioControllerTest {

    private final String BASE_URL = "/rebels";

    @MockBean
    private IReportService service;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Teste buscar rebelde pelo token")
    public void findRebeldToken() throws Exception {

        String percentualRebeldes = "75.0";
        String percentualTraidores = "25.0";
        Map<String, String> lista = new HashMap<String, String>();
            lista.put("Arma", "4.00");
        double pontosPerdidos = 4.0;

        Report relatorio = new Report();
        relatorio.setPercentageRebel(percentualRebeldes);
        relatorio.setPercentageTraitors(percentualTraidores);
        relatorio.setMediaRebelItems(lista);
        relatorio.setLostPointsTraitors(pontosPerdidos);

        when(service.montagemRelatorio()).thenReturn(relatorio);

        mockMvc.perform(get("/BASE_URL/relatorio")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
