package com.example.star_war.resistencia.negotiation;

import com.example.star_war.resistencia.involved.IFindInvolvedService;
import com.example.star_war.resistencia.involved.RebelsInvolvedDto;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RequiredArgsConstructor
public class GenerateNegotiation {

    private final IFindInvolvedService findInvolvedService;
    private final INegotiationService negotitationService;

    public List<Rebel> gerarNegociacao(RebelsInvolvedDto rebelsInvolvedDto) {

        List<Rebel> rebelListNegotiation = negotitationService.negotiationRebels(rebelsInvolvedDto);

        return rebelListNegotiation;
    }
}

