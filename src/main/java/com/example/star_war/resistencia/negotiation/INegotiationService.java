package com.example.star_war.resistencia.negotiation;

import com.example.star_war.resistencia.involved.RebelsInvolvedDto;
import com.example.star_war.resistencia.rebel.Rebel;

import java.util.List;

public interface INegotiationService {

    void validateScoreRebels(List<Rebel> rebelList);

    List<Rebel> negotiationRebels(RebelsInvolvedDto rebelsInvolvedDtos);
}
