package com.example.star_war.resistencia.negotiation;

import com.example.star_war.resistencia.exception.ConflictException;
import com.example.star_war.resistencia.exception.Messages;
import com.example.star_war.resistencia.involved.IFindInvolvedService;
import com.example.star_war.resistencia.involved.RebelsInvolvedDto;
import com.example.star_war.resistencia.item.Item;
import com.example.star_war.resistencia.rebel.IRebelRepository;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NegotiationService implements INegotiationService {

    private final IRebelRepository rebelRepository;
    private final IFindInvolvedService findInvolvedService;

    @Override
    public void validateScoreRebels(List<Rebel> rebelList) {

        int scoreRebel, flagRebel, scoreSolicitor,scoreRequested;
        scoreRebel = flagRebel = scoreSolicitor = scoreRequested= 0;

        for (Rebel rebel : rebelList) {
            for (Item itensRebel : rebel.getItens()) {
                scoreRebel += itensRebel.getValueItem();
            }
            if (flagRebel == 0){
                scoreSolicitor = scoreRebel;
            }else {
                scoreRequested = scoreRebel;
            }
            flagRebel++;
            scoreRebel = 0;
        }

        if (scoreSolicitor != scoreRequested) {
            throw new ConflictException(Messages.NEGOTIATION_CONFLICT_POINT);
        }
    }

    @Transactional
    public List<Rebel> negotiationRebels(RebelsInvolvedDto rebelsInvolvedDto) {

        List<Rebel> rebelList = findInvolvedService.findInvolvedRebel(rebelsInvolvedDto);

        this.validateScoreRebels(rebelList);

        Rebel solicitorRebel = rebelList.get(Messages.REBEL_SOLICITOR);
        List<Item> itensSolicitor = rebelList.get(Messages.REBEL_SOLICITOR).getItens();
        solicitorRebel.setItens(rebelList.get(Messages.REBEL_REQUESTED).getItens());

        Rebel requestedRebel = rebelList.get(Messages.REBEL_REQUESTED);
        requestedRebel.setItens(itensSolicitor);

        List<Rebel> listRebelResponse = new ArrayList<>();

        listRebelResponse.add(rebelRepository.save(solicitorRebel));
        listRebelResponse.add(rebelRepository.save(requestedRebel));

        return listRebelResponse;
    }
}
