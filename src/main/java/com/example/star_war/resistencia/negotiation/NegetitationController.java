package com.example.star_war.resistencia.negotiation;

import com.example.star_war.resistencia.involved.RebelsInvolvedDto;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rebels/negotiation")
@RequiredArgsConstructor
public class NegetitationController {

    private final INegotiationService service;

    @PostMapping()
    @ResponseBody
    public List<Rebel> negotiation(@Valid @RequestBody RebelsInvolvedDto rebelsInvolvedDto) {
        return service.negotiationRebels(rebelsInvolvedDto);
    }
}
