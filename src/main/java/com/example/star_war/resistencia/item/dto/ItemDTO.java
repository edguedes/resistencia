package com.example.star_war.resistencia.item.dto;

import com.example.star_war.resistencia.item.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemDTO {

    @Size(max = 20)
    private String resource;

    private int valueItem;

    public Item parseToItem() {
        Item item = new Item();
        item.setResource(this.getResource());
        item.setValueItem(this.getValueItem());
        return item;
    }
}
