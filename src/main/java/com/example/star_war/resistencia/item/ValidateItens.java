package com.example.star_war.resistencia.item;

import com.example.star_war.resistencia.exception.Messages;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
public class ValidateItens {

    public static Item valueItems(String valor) {

        Item itens = new Item();

        switch (valor) {
            case Messages.ITENS_ARMA:
                itens.setResource(Messages.ITENS_ARMA);
                itens.setValueItem(4);
                return itens;

            case Messages.ITENS_MUNICAO:
                itens.setResource(Messages.ITENS_MUNICAO);
                itens.setValueItem(3);
                return itens;

            case Messages.ITENS_AGUA:
                itens.setResource(Messages.ITENS_AGUA);
                itens.setValueItem(2);
                return itens;

            case Messages.ITENS_COMIDA:
                itens.setResource(Messages.ITENS_COMIDA);
                itens.setValueItem(1);
                return itens;
            default:
                return null;

        }
    }
}
