package com.example.star_war.resistencia.traitor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ITraitorRepository extends JpaRepository<Traitor, Long> {

    @Query(value = "SELECT COUNT(*) FROM rebel r JOIN traitor t "
            + "ON (r.rebel_id = t.id)"
            + "WHERE r.token = :token", nativeQuery = true)
    int findVoteDuplicate(@Param("token") String token);
}
