package com.example.star_war.resistencia.traitor;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/rebels/traitors")
@RequiredArgsConstructor
public class TraitorController {

    @Autowired
    private ITraidorService traitorService;

    @PostMapping()
    @ResponseBody
    public void informTraitor(@Valid @RequestBody InformantTraitorDTO informantTraitorDTO) {
        traitorService.updateTraidor(informantTraitorDTO);
    }
}
