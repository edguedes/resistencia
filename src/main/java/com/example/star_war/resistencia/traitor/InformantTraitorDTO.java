package com.example.star_war.resistencia.traitor;

import com.example.star_war.resistencia.involved.RebelsInvolvedDto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class InformantTraitorDTO {

    @NotBlank
    private String rebelInformant;

    @NotBlank
    private String rebelTraitor;

    public RebelsInvolvedDto parseToRebelsInvolvedDto(){
        RebelsInvolvedDto rebelsInvolvedDto = new RebelsInvolvedDto();
        rebelsInvolvedDto.setRebelSolicitor(this.getRebelInformant());
        rebelsInvolvedDto.setRebelRequested(this.getRebelTraitor());
        return  rebelsInvolvedDto;
    }
}
