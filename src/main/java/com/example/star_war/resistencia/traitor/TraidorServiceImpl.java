package com.example.star_war.resistencia.traitor;

import com.example.star_war.resistencia.exception.ConflictException;
import com.example.star_war.resistencia.exception.Messages;
import com.example.star_war.resistencia.involved.IFindInvolvedService;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TraidorServiceImpl implements ITraidorService {

    private final ITraitorRepository traidorRepository;
    private final IFindInvolvedService findInvolvedService;

    @Override
    public void updateTraidor(InformantTraitorDTO informantTraitorDTO) {

        List<Rebel> rebelList = findInvolvedService.findInvolvedRebel(informantTraitorDTO.parseToRebelsInvolvedDto());

        int voteInformant = traidorRepository
                .findVoteDuplicate(rebelList.get(Messages.REBEL_SOLICITOR).getToken());

        if (voteInformant != 0){
            throw new ConflictException(Messages.REBEL_CONFLICT_VOTO);
        }

        rebelList.get(Messages.REBEL_REQUESTED)
                .setScore(rebelList.get(Messages.REBEL_REQUESTED).getScore() + 1);

        if (rebelList.get(Messages.REBEL_REQUESTED).getScore() >= 3){
            rebelList.get(Messages.REBEL_REQUESTED)
                    .setTraitor(true);
        }

        Traitor traitor = new Traitor();
        traitor.setInformant( rebelList.get(Messages.REBEL_SOLICITOR));
        traitor.setTraitor( rebelList.get(Messages.REBEL_REQUESTED));

        traidorRepository.save(traitor);
    }
}
