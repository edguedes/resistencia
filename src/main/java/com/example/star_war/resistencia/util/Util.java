package com.example.star_war.resistencia.util;

import java.text.Normalizer;

public class Util {

    public static String removeSpecialCharacters(String input) {
        String inputAscRemove = Normalizer.normalize(input, Normalizer.Form.NFD)
                .replaceAll("[^ a-zA-Z0-9_-]", "").trim();
        return inputAscRemove;
    }
}