package com.example.star_war.resistencia.location;

import com.example.star_war.resistencia.exception.Messages;
import com.example.star_war.resistencia.exception.NotFoundException;
import com.example.star_war.resistencia.item.ValidateItens;
import com.example.star_war.resistencia.rebel.IRebelRepository;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LocalServiceUpdate implements ILocalServiceUpdate {

    private final IRebelRepository rebelRepository;
    private final ILocationRepository locationRepository;
    private final ValidateItens validateItens;

    @Override
    public Location updateLocal(Location localizacao, String token) {

        Optional<Rebel> rebelOpt = Optional.ofNullable(rebelRepository.findByToken(token)
                .orElseThrow(() -> new NotFoundException(Messages.REBEL_NOT_FOUND)));

        rebelOpt.get().setLocation(localizacao);
        rebelRepository.save(rebelOpt.get());

        return rebelOpt.get().getLocation();
    }
}
