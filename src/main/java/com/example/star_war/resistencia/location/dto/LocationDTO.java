package com.example.star_war.resistencia.location.dto;

import com.example.star_war.resistencia.location.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocationDTO {

    private Long id;

    @NotNull
    @Size(max = 60)
    private String base;

    @NotNull
    private BigDecimal latitude;

    @NotNull
    private BigDecimal longitude;

    public Location parseToLocation() {
        Location location  = new Location();
        location.setBase(this.getBase());
        location.setLatitude(this.getLatitude());
        location.setLongitude(this.getLongitude());
        return location;
    }
}
