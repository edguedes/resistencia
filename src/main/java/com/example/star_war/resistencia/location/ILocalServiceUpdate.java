package com.example.star_war.resistencia.location;

public interface ILocalServiceUpdate {

    Location updateLocal(Location location, String token);
}
