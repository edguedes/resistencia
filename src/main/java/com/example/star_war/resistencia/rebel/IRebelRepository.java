package com.example.star_war.resistencia.rebel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IRebelRepository extends JpaRepository<Rebel, Long> {

    Optional<Rebel> findByToken(String token);

    List<Rebel> findByTraitor(boolean traitor);

    Optional<Rebel> findByTokenAndTraitor(String token, boolean traitor);
}
