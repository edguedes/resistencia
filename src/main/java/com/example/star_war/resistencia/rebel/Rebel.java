package com.example.star_war.resistencia.rebel;

import com.example.star_war.resistencia.item.Item;
import com.example.star_war.resistencia.location.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rebel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rebel_id")
    private Long id;

    @NotBlank
    @Size(max = 40)
    private String name;

    private String token;

    @NotNull
    private int age;

    private int score = 0;

    private boolean traitor = false;

    @NotBlank
    @Size(max = 1)
    private String genre;

    @OneToOne(cascade = CascadeType.ALL)
    @Valid
    private Location location;

    @OneToMany(cascade = CascadeType.ALL)
    List<Item> itens = new ArrayList<>();
}
