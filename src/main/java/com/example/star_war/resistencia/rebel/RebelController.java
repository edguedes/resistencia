package com.example.star_war.resistencia.rebel;

import com.example.star_war.resistencia.rebel.dto.RebelDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rebels")
@RequiredArgsConstructor
public class RebelController {

    private final IRebelService rebelService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@Valid @RequestBody RebelDTO rebelDTO) {
        rebelService.create(rebelDTO);
    }

    @GetMapping()
    @ResponseBody
    public List<Rebel> findAllRebel() { return rebelService.findAllRebel(); }

    @GetMapping(value = "/list-traitors")
    @ResponseBody
    public List<Rebel> findAllRebelTraitor() { return rebelService.findAllTraitor(); }

    @GetMapping(value = "/{token}")
    @ResponseBody
    public Rebel findRebelToken(@PathVariable String token) {
        return rebelService.findRebel(token);
    }
}
