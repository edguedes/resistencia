package com.example.star_war.resistencia.rebel;

import com.example.star_war.resistencia.exception.BadRequestException;
import com.example.star_war.resistencia.exception.Messages;

import java.util.Arrays;
import java.util.UUID;

public class ConfigRebel {

    private static String genre[] = {"M", "F" };

    public static boolean isValidateGenre(String sexo) {
        if (!Arrays.stream(genre).anyMatch(sexo::equals)){
            throw new BadRequestException(Messages.REBEL_GENRE);
        }
        return true;
    }

    public static String createToken(){ return UUID.randomUUID().toString().replace("-", ""); }
}
