package com.example.star_war.resistencia.rebel;

import com.example.star_war.resistencia.item.Item;
import com.example.star_war.resistencia.item.ValidateItens;
import com.example.star_war.resistencia.rebel.dto.RebelDTO;
import com.example.star_war.resistencia.util.Util;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class RebelServiceImpl implements IRebelService {

    @Autowired
    private IRebelRepository repository;

    @Autowired
    private ValidateItens validateItens;

    @Override
    public void create(RebelDTO rebelDTO) {

        Rebel rebel = new Rebel();
            rebel = rebelDTO.parseToRebel();

        ConfigRebel.isValidateGenre(rebel.getGenre());

        rebel.setName(Util.removeSpecialCharacters(rebel.getName()));
        rebel.getLocation().setBase(Util.removeSpecialCharacters(rebel.getLocation().getBase()));
        rebel.setToken(ConfigRebel.createToken());

        List<Item> newItens = new ArrayList<>();

        rebel.getItens().forEach(item -> {
            Item itensValue = ValidateItens.valueItems(item.getResource());
            if (itensValue != null) {
                newItens.add(itensValue);
           }
        });

        rebel.setItens(newItens);
        repository.save(rebel);
    }

    @Override
    public List<Rebel> findAllRebel() { return repository.findByTraitor(false); }

    @Override
    public List<Rebel> findAllTraitor() {
        return repository.findByTraitor(true);
    }

    @Override
    public Rebel findRebel(String token) {
        return repository.findByTokenAndTraitor(token, false).get();
    }
}
