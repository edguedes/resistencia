package com.example.star_war.resistencia.rebel;

import com.example.star_war.resistencia.rebel.dto.RebelDTO;

import java.util.List;

public interface IRebelService {

    void create(RebelDTO rebelDTO);

    List<Rebel> findAllRebel();

    List<Rebel> findAllTraitor();

    Rebel findRebel(String token);
}
