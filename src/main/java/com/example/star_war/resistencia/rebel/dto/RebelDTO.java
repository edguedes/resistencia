package com.example.star_war.resistencia.rebel.dto;

import com.example.star_war.resistencia.item.Item;
import com.example.star_war.resistencia.item.dto.ItemDTO;
import com.example.star_war.resistencia.location.dto.LocationDTO;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RebelDTO {

    private Long id;

    @NotBlank
    @Size(max = 40)
    private String name;

    private String token;

    @NotNull
    private int age;

    private int score = 0;

    private boolean traitor = false;

    @NotBlank
    @Size(max = 1)
    private String genre;

    @Valid
    private LocationDTO locationDTO;


    List<ItemDTO> itensDTO = new ArrayList<>();

    public Rebel parseToRebel() {
        Rebel rebel = new Rebel();
        rebel.setName(this.getName());
        rebel.setToken(this.getToken());
        rebel.setAge(this.getAge());
        rebel.setScore(this.getScore());
        rebel.setGenre(this.getGenre());
        rebel.setLocation(this.getLocationDTO().parseToLocation());
        rebel.setItens(this.parseToItem(itensDTO));
        return rebel;
    }

    public List<Item> parseToItem(List<ItemDTO> itenDTOS) {
        List<Item> itensList = new ArrayList<>();

        itenDTOS.forEach(itemDTO -> {
            itensList.add(itemDTO.parseToItem());
        });
        return itensList;
    }
}
