package com.example.star_war.resistencia.report;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReportServiceImpl implements IReportService {

    private final IPercentualTraidor percentualTraidor;
    private final IPointsTraitors pointsTraitors;
    private final IPercentualServiceRebeldes percentualRebeldes;
    private final IListItens listItens;

    public Report montagemRelatorio() {
        final Report report = new Report();

        report.setPercentageRebel(percentualRebeldes.percentualRebeldes());
        report.setPercentageTraitors(percentualTraidor.percentualTraidor());
        report.setMediaRebelItems(listItens.listItens());
        report.setLostPointsTraitors(pointsTraitors.PointsTraitors());

        return report;
    }
}
