package com.example.star_war.resistencia.report;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rebels/reports")
@RequiredArgsConstructor
public class ReportController {

    @Autowired
    private IReportService reportService;

    @GetMapping()
    @ResponseBody
    public Report generateAllReports() {
        return reportService.montagemRelatorio();
    }
}
