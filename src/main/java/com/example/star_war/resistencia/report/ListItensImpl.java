package com.example.star_war.resistencia.report;

import com.example.star_war.resistencia.exception.Messages;
import com.example.star_war.resistencia.rebel.IRebelService;
import com.example.star_war.resistencia.rebel.Rebel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ListItensImpl implements IListItens{

    @Autowired
    private IRebelService rebelService;

    DecimalFormat formatador = new DecimalFormat("0.00");

    @Override
    public Map<String, String> listItens() {
        List<Rebel> rebeldes = rebelService.findAllRebel();

        Map<String, String> lista = new HashMap<String, String>();

        if ( rebeldes.size() == 0 ){
            return lista;
        }

        double totalRebeldes = rebeldes.size();
        int qtdArma = 0;
        int qtdMunicao = 0;
        int qtdAgua = 0;
        int qtdComida = 0;

        for (Rebel rebelde : rebeldes) {
            for (int j = 0; j < rebelde.getItens().size(); j++) {
                if (Messages.ITENS_MUNICAO.equals(rebelde.getItens().get(j).getResource())) {
                    qtdMunicao++;
                }
                if (Messages.ITENS_ARMA.equals(rebelde.getItens().get(j).getResource())) {
                    qtdArma++;
                }
                if (Messages.ITENS_COMIDA.equals(rebelde.getItens().get(j).getResource())) {
                    qtdAgua++;
                }
                if (Messages.ITENS_AGUA.equals(rebelde.getItens().get(j).getResource())) {
                    qtdComida++;
                }
            }
        }

        if (qtdArma == 0) {
            lista.put(Messages.ITENS_ARMA, "0.00");
        } else {
            double valor = qtdArma / totalRebeldes;
            String result = formatador.format(valor);
            lista.put(Messages.ITENS_ARMA, result);
        }
        if (qtdMunicao == 0) {
            lista.put(Messages.ITENS_MUNICAO, "0.00");
        } else {
            double valor = qtdMunicao / totalRebeldes;
            String result = formatador.format(valor);
            lista.put(Messages.ITENS_MUNICAO, result);
        }
        if (qtdAgua == 0) {
            lista.put(Messages.ITENS_COMIDA, "0.00");
        } else {
            double valor = qtdAgua / totalRebeldes;
            String result = formatador.format(valor);
            lista.put(Messages.ITENS_COMIDA, result);
        }
        if (qtdAgua == 0) {
            lista.put(Messages.ITENS_AGUA, "0.00");
        } else {
            double valor = qtdComida / totalRebeldes;
            String result = formatador.format(valor);
            lista.put(Messages.ITENS_AGUA, result);
        }
        return lista;
    }
}
