package com.example.star_war.resistencia.report;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Report {

    private String percentageTraitors;

    private String percentageRebel;

    Map<String,String> mediaRebelItems = new HashMap<String,String>();

    private double lostPointsTraitors;
}
