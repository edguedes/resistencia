package com.example.star_war.resistencia.report;

import com.example.star_war.resistencia.rebel.IRebelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
public class PercentualServiceRebeldesImpl implements IPercentualServiceRebeldes {

    @Autowired
    private IRebelRepository rebelRepository;

    DecimalFormat formatador = new DecimalFormat("0.00");


    @Override
    public String percentualRebeldes() {
        int totalRebeldes = rebelRepository.findByTraitor(false).size();
        int totalGeral = rebelRepository.findAll().size();

        if (totalRebeldes == 0)
            return "0.00 %";

        Double percent = (((double)totalRebeldes / (double)totalGeral ) * 100);
        String result = formatador.format(percent);

        return result + " %";
    }
}
