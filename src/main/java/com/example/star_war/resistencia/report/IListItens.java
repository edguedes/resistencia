package com.example.star_war.resistencia.report;

import java.util.Map;

public interface IListItens {

    Map<String, String> listItens();
}
