package com.example.star_war.resistencia.report;

import com.example.star_war.resistencia.rebel.IRebelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
public class PercentualTraidorImpl implements IPercentualTraidor{

    @Autowired
    private IRebelRepository rebelRepository;

    @Override
    public String percentualTraidor() {
        int totalRebeldes = rebelRepository.findAll().size();
        int totalTraidores = rebelRepository.findByTraitor(true).size();

        if (totalTraidores == 0)
            return "0.00 %";

        double percentual = ((double)totalTraidores / (double)totalRebeldes) * 100;

        DecimalFormat formatador = new DecimalFormat("0.00");
        String resultado = formatador.format(percentual);

        return resultado + " %";
    }
}
