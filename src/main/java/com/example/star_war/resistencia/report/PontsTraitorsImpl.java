package com.example.star_war.resistencia.report;

import com.example.star_war.resistencia.rebel.IRebelService;
import com.example.star_war.resistencia.rebel.Rebel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PontsTraitorsImpl implements IPointsTraitors {

    @Autowired
    private IRebelService rebelService;

    @Override
    public int PointsTraitors() {
        List<Rebel> traidores = rebelService.findAllTraitor();

        int totalTraidores = traidores.size();
        int pontosPerdidos = 0;
        int i = 0;

        if (totalTraidores == 0)
            return 0;

        for (Rebel traidor : traidores) {
            pontosPerdidos = +traidor.getItens().get(i).getValueItem();
            i++;
        }
        return pontosPerdidos;
    }
}
