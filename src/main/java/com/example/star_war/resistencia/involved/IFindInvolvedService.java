package com.example.star_war.resistencia.involved;

import com.example.star_war.resistencia.rebel.Rebel;

import java.util.List;

public interface IFindInvolvedService {

    List<Rebel> findInvolvedRebel(RebelsInvolvedDto rebelsInvolvedDto);
}
