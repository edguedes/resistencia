package com.example.star_war.resistencia.involved;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class RebelsInvolvedDto {

    @NotBlank
    private String rebelSolicitor;

    @NotBlank
    private String rebelRequested;

}
