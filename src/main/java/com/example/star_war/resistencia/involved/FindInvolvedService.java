package com.example.star_war.resistencia.involved;

import com.example.star_war.resistencia.exception.Messages;
import com.example.star_war.resistencia.exception.NotFoundException;
import com.example.star_war.resistencia.rebel.IRebelRepository;
import com.example.star_war.resistencia.rebel.Rebel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Service
public class FindInvolvedService implements IFindInvolvedService {

    @Autowired
    private IRebelRepository rebelRepository;

    public List<Rebel> findInvolvedRebel(RebelsInvolvedDto rebelsInvolvedDto){

        List<Rebel> rebelList = new ArrayList<>();

        Rebel rebelSolicitor = rebelRepository.findByTokenAndTraitor(rebelsInvolvedDto.getRebelSolicitor(), false)
                .orElseThrow(() ->new NotFoundException(Messages.REBEL_NOT_FOUND));

        Rebel rebelRequested = rebelRepository.findByTokenAndTraitor(rebelsInvolvedDto.getRebelRequested(), false)
                .orElseThrow(() ->new NotFoundException(Messages.REBEL_NOT_FOUND));

        rebelList.add(rebelSolicitor);
        rebelList.add(rebelRequested);

        return rebelList;
    }

}
