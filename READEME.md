Star Wars Resistence
O império continua sua luta incessante de dominar a galáxia, tentando ao máximo expandir seu território e eliminar rebeldes.
Você, como um soldado de resistência, foi designado para desenvolver um sistema para compartilhar recursos entre rebeldes.
Star Wars Resistence  - é uma API que tem a finalidade de genrenciamento de Rebeldes. Como:

Cadastrar um novo rebelde
Atualizar localização do rebelde
Informar rebelde traidor
Gerar relatório
Realizar troca entre rebeldes
Listar todos rebeldes
Buscar rebelde pelo token

Autenticação
A API não presica de autenticação prévia. Apenas seguir as regras definida na SLA.
Error Codes
Pricipais status usados na API
201 – Created
Confirmação de salvamento de um objeto
200 – Ok
Confirmação de ação
400 – Bad Request
Dados enviados de forma incorreta ou fora do padrão
409 – Conflict
Conflitos gerados na quebra da regra de negócio
404 – Not Found
Dados não encontrados na base de dados
Tecnologias
*Java *Spring boot *Spring Data *Hibernate
*PpostgresSql *Maven

POST http://localhost:8080/rebels

{
    "nome"  :   "Carla Munhos Leite",
    "idade"   :   27,
    "sexo" :   "F",
    "localizacao" : {
        "base" : "Asteroide B",
        "latitude" : "254783",
        "longitude" : "456247" 
    },
    "itens" : [{
                "nome" : "Água",
                "ponto": 5
            },
            {
                "nome" : "Munição",
                "ponto": 4
            }]
}

**Atualizar posição rebelde **

PUT http://localhost:8080/rebels/token/e15d65736936468d92033b971573ec33
Atualizar localição

{
 "base" : "Planeta M28",
  "latitude" : "1584524",
  "longitude" : "1458569" 
}

POST http://localhost:8080/rebels/voto-traidor
Token de um rebelde Informante e do Rebelde Traidor
{
    "tokenRebelde_1" : "e402ae1c7a0f4d52af33aa60218cd045",
    "tokenRebelde_2" : "fa98467820244c9aaaa636d3265162ad"
}

GET http://localhost:8080/rebels/relatorio
Relatório gestão que permite crescimento

{
    "percentualTraidores": "20,00 %",
    "percentualRebeldes": "80,00 %",
    "mediaItensRebeldes": {
        "Comida": "6,00",
        "Municao": "0.00",
        "Arma": "1,00",
        "Agua": "3,00"
    },
    "pontosPerdidosTraidores": 6.0
}

POST http://localhost:8080/rebels/trocar
Negociação entre rebeldes

{
    "tokenRebelde_1" : "5fb3bc39d9324d378fe5e9438b222f03",
    "tokenRebelde_2" : "01191735755c4441b856e28332a32726"
}

**Busca de lideres **

GET http://localhost:8080/rebels/ - Lista todos os rebeldes

GET http://localhost:8080/rebels/token/{token} - Busca por rebelde pelo token
